//
//  User.h
//  Coding
//
//  Created by LC on 16/11/12.
//  Copyright © 2016年 LC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject<NSCoding>

@property (nonatomic ,strong) NSString *name;
@property (nonatomic ,assign) BOOL sex;
@property (nonatomic ,assign) NSInteger age;

@end
