//
//  ToolHelper.m
//  Coding
//
//  Created by LC on 16/11/12.
//  Copyright © 2016年 LC. All rights reserved.
//

#import "ToolHelper.h"

@implementation ToolHelper

// 保存路径
+ (NSString *)pathWithName:(NSString *)name
{
    NSArray *documents =NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *path = [documents[0] stringByAppendingPathComponent:name];
    return path;
}

+ (BOOL)fileExists:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path isDirectory:nil])
        return YES;
    else
        return NO;
}

// 序列化保存
+ (void)archiveDataWithPath:(NSString *)path Content:(id)content
{
    [NSKeyedArchiver archiveRootObject:content toFile:path];
}

// 序列化读取
+ (id)readArchiveDataWithPath:(NSString *)path
{
    return [NSKeyedUnarchiver unarchiveObjectWithFile:path];
}

// 直接写入文件
+ (void)writeDataWithPath:(NSString *)path Content:(id)content
{
    [content writeToFile:path atomically:YES];
}

// 直接写入文件
+ (void)writeDataWithPath:(NSString *)path Array:(NSArray *)array
{
    [array writeToFile:path atomically:YES];
}

+ (void)writeDataWithPath:(NSString *)path Dictionary:(NSDictionary *)dictionary
{
    [dictionary writeToFile:path atomically:YES];
}

+ (BOOL)writeDataWithPath:(NSString *)path String:(NSString *)string
{
    NSError *error=nil;
    NSString *floder = [path substringToIndex:[path rangeOfString:@"/" options:NSBackwardsSearch ].location];
    NSFileManager *fileManger=[NSFileManager defaultManager];
    if (![fileManger fileExistsAtPath:floder] ) {
        [fileManger createDirectoryAtPath:floder withIntermediateDirectories:YES attributes:nil error:&error];
    }
    return [string writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
}

+ (void)writeDataWithPath:(NSString *)path Data:(NSData *)data
{
    [data writeToFile:path atomically:YES];
}

// 直接读取文件
+ (NSArray *)readArrayWithPath:(NSString *)path
{
    return [NSArray arrayWithContentsOfFile:path];
}

+ (NSDictionary *)readDictionaryWithPath:(NSString *)path
{
    return [NSDictionary dictionaryWithContentsOfFile:path];
}

+ (NSString *)readStringWithPath:(NSString *)path
{
    if (![ToolHelper fileExists:path]) {
        return nil;
    }
    NSError *error = nil;
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        LC_NSLog(@"path '%@' error",path);
        return nil;
    }
    return content;
}

+ (NSData *)readDataWithPath:(NSString *)path
{
    if (![ToolHelper fileExists:path]) {
        LC_NSLog(@"%@ 不存在",path);
        return nil;
    }
    return [NSData dataWithContentsOfFile:path];
}

// 字典转json字符串
+ (NSString *)JSONstringWithDictionary:(NSDictionary *)dictionary
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    if (error) {
        LC_NSLog(@"字典转json字符串失败");
        return nil;
    }
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

// 16进制颜色转化为uicolor
+(UIColor *)translateHexStringToColor:(NSString *)hexColor
{
    NSString *cString = [[hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    if ([cString length] < 6) return [UIColor blackColor];
    
    if ([cString hasPrefix:@"0X"])  cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])   cString = [cString substringFromIndex:1];
    if ([cString length] != 6) return [UIColor blackColor];
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rColorValue = [cString substringWithRange:range];
    range.location = 2;
    NSString *gColorValue = [cString substringWithRange:range];
    range.location = 4;
    NSString *bColorValue = [cString substringWithRange:range];
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rColorValue] scanHexInt:&r];
    [[NSScanner scannerWithString:gColorValue] scanHexInt:&g];
    [[NSScanner scannerWithString:bColorValue] scanHexInt:&b];
    return [UIColor colorWithRed:((CGFloat) r / 255.0f) green:((CGFloat) g / 255.0f) blue:((CGFloat) b / 255.0f) alpha:1.0f];
}

// 格式化时间日期
+(NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息 +0000。
    [dateFormatter setDateFormat:format];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}


@end
