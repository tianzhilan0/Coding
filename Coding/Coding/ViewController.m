//
//  ViewController.m
//  Coding
//
//  Created by LC on 16/11/12.
//  Copyright © 2016年 LC. All rights reserved.
//

#import "ViewController.h"
#import "User.h"
#import "ToolHelper.h"

#define userInfo @"user.text"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
    //1、一个user
    //归档
    User *zhang = [[User alloc] init];
    NSString *path = [ToolHelper pathWithName:userInfo];
    [NSKeyedArchiver archiveRootObject:zhang toFile:path];
    
    //解档
    User *user = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    NSLog(@"%@", user.name);
    */
    
    
    
    //2、多个user
    
    User *li = [[User alloc] init];
    li.name = @"李四";
    
    User *wang = [[User alloc] init];
    wang.name = @"王五";
    NSString *path = [ToolHelper pathWithName:userInfo];
    
    NSData *users = [NSKeyedArchiver archivedDataWithRootObject:@[li,wang]];
    [NSKeyedArchiver archiveRootObject:users toFile:path];
    
    
    NSData *data = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSLog(@"%@",array);
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
