//
//  User.m
//  Coding
//
//  Created by LC on 16/11/12.
//  Copyright © 2016年 LC. All rights reserved.
//

#import "User.h"

@implementation User

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"张三";
        self.age = 22;
        self.sex = 1;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.sex = [aDecoder decodeBoolForKey:@"sex"];
        self.age = [aDecoder decodeIntegerForKey:@"age"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeBool:self.sex forKey:@"sex"];
    [aCoder encodeInteger:self.age forKey:@"age"];
}



@end
