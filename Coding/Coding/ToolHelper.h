//
//  ToolHelper.h
//  Coding
//
//  Created by LC on 16/11/12.
//  Copyright © 2016年 LC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#ifdef DEBUG
#define LC_NSLog(FORMAT, ...) fprintf(stderr,"%s:%d\t%s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(...)
#endif

@interface ToolHelper : NSObject

// 保存路径
+ (NSString *)pathWithName:(NSString *)name;

// 文件是否存在
+(BOOL)fileExists:(NSString *)path;

// 序列化保存
+ (void)archiveDataWithPath:(NSString *)path Content:(id)content;

// 序列化读取
+ (id)readArchiveDataWithPath:(NSString *)path;

// 直接写入文件
+ (void)writeDataWithPath:(NSString *)path Content:(id)content;

+ (void)writeDataWithPath:(NSString *)path Array:(NSArray *)array;
+ (void)writeDataWithPath:(NSString *)path Dictionary:(NSDictionary *)dictionary;
+ (BOOL)writeDataWithPath:(NSString *)path String:(NSString *)string;
+ (void)writeDataWithPath:(NSString *)path Data:(NSData *)data;

// 直接读取文件
+ (NSArray *)readArrayWithPath:(NSString *)path;
+ (NSDictionary *)readDictionaryWithPath:(NSString *)path;
+ (NSString *)readStringWithPath:(NSString *)path;
+ (NSData *)readDataWithPath:(NSString *)path;

// 字典转json字符串
+ (NSString *)JSONstringWithDictionary:(NSDictionary *)dictionary;

// 16进制颜色转化为uicolor
+(UIColor *)translateHexStringToColor:(NSString *)hexColor;

/*!
 *  格式化时间日期
 *
 *  @param date   <#date description#>
 *  @param format yyyyMMddHHmmssSSS
 *
 *  @return <#return value description#>
 */
+(NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format;




@end
